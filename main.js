var cinemas = {

    "Embajador": {
        "title": "Cinema Embajador",
        "telefono": "5090909",
        "direccion": "cr 5 #24.66",
        "   ": [{
                "nombre": "Annabelle",
                // "elenco": ["Madison Ileman", "Vera Farmiga", "Patrick Wilson", "Michael Cimino"],
                "actorPrincipal": "Madison Ileman",
                "clasificacion": "familiar mayores de 12 años",
                "genero": "Terror",
                "duracion": "125 minutos",
                "director": "John R. Leonetti",
                "idioma": "Inglés",
            },
            {
                "nombre": "Toy story 4",
                // "elenco": ["Tom Hanks", "Tim Allen", "Joan Cusack", "Estelle Harris", "Jeff Pidgeon"],
                "actorPrincipal": "Madison Ileman",
                "clasificacion": "familiar",
                "genero": "Comedia",
                "duracion": "100 minutos",
                "director": "Josh Cooley",
                "idioma": "Inglés",
            }
        ],
    },
    "Galerias": {
        "title": "Cinema Embajador",
        "telefono": "5090909",
        "direccion": "cr 24 #54.66",
        "peliculas": [{
                // "elenco": ["James Earl Jones como Mufasa", "Billy Eichner en el papel de Timón ", "Seth Rogen como Pumba"],
                "nombre": "El Rey León",
                "actorPrincipal": "Madison Ileman",
                "clasificacion": "familiar",
                "genero": "Acción real, Animación",
                "duracion": "125 minutos",
                "director": "Jon Favreau",
                "idioma": "Inglés",
                // "horario": ["13:00 pm -17:00 pm", "15:00 pm - 19:00 pm", "18:00 pm - 22:00 pm", "20:00 pm - 23:00 pm"]
            },
            {
                "nombre": "Spiderman",
                // "elenco": ["Tom Holland", "Samuel L. Jackson", "Zendaya", "Cobie Smulders", "Jon Favreau"],
                "actorPrincipal": "Tom Holland",
                "clasificacion": "Mayores de 7 años",
                "genero": "Acción",
                "duracion": "130 minutos",
                "director": "Jon Watts",
                "idioma": "Inglés",
                // "horario": ["13:00 pm -17:00 pm", "15:00 pm - 19:00 pm", "20:00 pm - 22:00 pm"]
            }
        ],
    }
}


function start() {
    title = document.getElementById('title');
    tdAddress = document.getElementById('tdAddress');
    tdPhone = document.getElementById('tdPhone');
    trParent = document.getElementById('trParent');
    pelicula = document.getElementById('pelicula');
    protagonista = document.getElementById('protagonista');
    clasificación = document.getElementById('clasificación');
    genero = document.getElementById('genero');
    duración = document.getElementById('duración');
    director = document.getElementById('director');
    idioma = document.getElementById('idioma');
    tblMovies = document.getElementById('tblMovies');
    inpNombre = document.getElementById('inpNombre').value;
    inpPrincipalAct = document.getElementById('inpPrincipalAct').value;
    inpDirector = document.getElementById('inpDirector').value;
    inpGenero = document.getElementById('inpGenero').value;
    inpDuracion = document.getElementById('inpDuracion').value;
    inpClasificacion = document.getElementById('inpClasificacion').value;
    inpIdioma = document.getElementById('inpIdioma').value;

    btnAgregar = document.getElementById('btnAgregar').addEventListener('click', addMovie);
    trParent.innerHTML = "";
    mostrarJson();

    console.log('Cinema Embajador - Pelicula 1 : ' + cinemas.Embajador.peliculas[0].nombre);

}
var peliculas = cinemas.Embajador.peliculas;


function addMovie() {
    console.log('llama addMovie ');
    console.log('inpNombre : ' + document.getElementById('inpNombre').value);
    let newMovie = {
        "nombre": document.getElementById('inpNombre').value,
        "actorPrincipal": document.getElementById('inpPrincipalAct').value,
        "clasificacion": document.getElementById('inpClasificacion').value,
        "genero": document.getElementById('inpGenero').value,
        "duracion": document.getElementById('inpDuracion').value,
        "director": document.getElementById('inpDirector').value,
        "idioma": document.getElementById('inpIdioma').value
    }
    peliculas.push(newMovie);
    tr = document.getElementById('tblMovies');
    tr.lastElementChild.innerHTML = "";
    tr.lastElementChild.innerHTML = "";
    console.log('tr.childNodes.length : ' + tr.childNodes.length);
    let count1 = tr.childNodes.length - 1;
    let count = tr.childNodes.length;
    // trParent.innerHTML = "";
    tr.removeChild(tr.childNodes[2]);
    // do {
    //     console.log('count : ' + count);
    //     count--;
    //     console.log('count : ' + count);
    // } while (count <= 0);
    // while (tr.childNodes.length > 1) {

    //     tr.lastChild.remove;
    // }

    // tr.lastChild.innerHTML = "";
    // console.log("document.getElementById('tblMovies').children : " + document.getElementById('tblMovies').children);
    // tr = document.getElementById('tblMovies').children[1].innerHTML = "";

    mostrarJson();

}

function mostrarJson(e) {

    title.innerHTML = cinemas.Embajador.title;
    tdAddress.innerHTML = cinemas.Embajador.direccion;
    tdPhone.innerHTML = cinemas.Embajador.telefono;

    let parent = document.getElementById('tblParent');
    for (let i = 0; i < peliculas.length; i++) {
        var tr = document.createElement('tr');

        tblMovies.appendChild(tr);

        let text = i + 1;
        let td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].nombre;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].actorPrincipal;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].clasificacion;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].genero;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].duracion;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].director;
        td = createTd(text);
        tr.appendChild(td);

        text = cinemas.Embajador.peliculas[i].idioma;
        td = createTd(text);
        tr.appendChild(td);
    }
}

function createTd(text) {
    td = document.createElement('td');
    let content = document.createTextNode(text);
    td.appendChild(content);
    return td;
}

function showMovies(peliculas) {
    for (let i = 0; i < peliculas.length; i++) {


    }
}




function addCampo() {
    let newCampo = prompt('Nombre del campo');

    let valueCampo = prompt('Valor del campo');


    cinemas.newCampo = valueCampo;
    pintaNewCampo(newCampo);


}

function pintaNewCampo(newCampo) {

    console.log('newCampo : ' + newCampo);
    nombre.innerHTML += cinemas[0].nombre;
    nomPeli.innerHTML += cinemas[0].cartelera[0].pelicula;
    clasificacion.innerHTML = cinemas[0].cartelera[0].clasificacion;
    genero.innerHTML = cinemas[0].cartelera[0].genero;
    director.innerHTML = cinemas[0].cartelera[0].director;
    idioma.innerHTML = cinemas[0].cartelera[0].idioma;
    duracion.innerHTML = cinemas[0].cartelera[0].duracion;

    compoNew.innerHTML += `${newCampo}`;
    compoNew.style.display = "block";

    // compoNew.innerHTML += cinemas[0].newCampo;
}



window.addEventListener('load', start);